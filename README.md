# my-project

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

/Users/fieldfang/jenkins_home/workspace/zipBlog

```bash
cd /var/jenkins_home/tools/jenkins.plugins.nodejs.tools.NodeJSInstallation/nodejs_9.5.0

./bin/node -v     # 可以正常的输出node版本
```

docker stop zipBlog || true \
 && docker rm zipBlog || true \
 && cd /Users/fieldfang/jenkins_home/workspace/zipBlog  \
 && docker build  -t zipBlog  . \
 && docker run -d -p 8083:80 --name zipBlog -v /Users/fieldfang/jenkins_home/workspace/zipBlog/dist:/usr/share/nginx/html -v /Users/fieldfang/jenkins_home/workspace/zipBlog/nginx.conf:/etc/nginx/nginx.conf zipBlog



docker stop zipBlog || true \
 && docker rm zipBlog || true \
 && cd /Users/fieldfang/jenkins_home/workspace/zipBlog  \
 && docker build  -t zipBlog  . \
 && docker run -d -p 8083:80 --name zipBlog -v /Users/fieldfang/jenkins_home/workspace/zipBlog/dist:/usr/share/nginx/html -v /Users/fieldfang/jenkins_home/workspace/zipBlog/zipBlog/nginx.conf:/etc/nginx/nginx.conf zipBlog
